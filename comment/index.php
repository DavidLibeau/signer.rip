<!DOCTYPE html>

<html lang="fr">

<head>
    <?php
    $titre="Comment signer le #RéférendumADP ?";
    $desctription="Page regroupant tous les tutoriels et aides diverses pour signer le référendum ADP.";
    ?>
    <meta charset="utf-8" />
    <title><?php echo($titre); ?></title>
    <meta property="og:title" content="<?php echo($titre); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://signer.rip/" />
    <meta property="og:image" content="https://signer.rip/image.png" />
    <meta property="og:description" content="<?php echo($desctription); ?>" />
    <meta name="description" content="<?php echo($desctription); ?>" />
    <meta name="keywords" content="signer, référendum, ADP, RIP, aéroport de paris, aéroport, paris, privatisation" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@DavidLibeau" />
    <meta name="twitter:creator" content="@DavidLibeau" />
    <meta name="twitter:title" content="<?php echo($titre); ?>" />
    <meta name="twitter:description" content="<?php echo($desctription); ?>" />
    <meta name="twitter:image" content="https://signer.rip/image.png" />
    <meta property="og:locale" content="fr_FR" />
    <link rel="shortcut icon" type="image/png" href="https://signer.rip/twemoji-signer.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="theme-color" content="#3c3c3c" />
    <link rel="stylesheet" href="https://dav.li/forkawesome/1.1.7/css/fork-awesome.min.css" />
    <style>
        body, html {
            margin: 0;
            font-family: sans-serif;
            background-color: #fdfdfd;
            line-height: 20px;
        }
        header {
            padding: 5vh 20px;
            background-color: rgba(78,110,186,1);
            -webkit-box-shadow:inset 0 0 50px 0 rgba(0,0,0,0.2);
            box-shadow:inset 0 0 50px 0 rgba(0,0,0,0.2);
        }
        header h1{
            text-align: center;
            line-height: 40px;
            color: white;
        }
        a {
            color: inherit;
            font-weight: bold;
            text-decoration: none;
        }
        a:hover{
            text-decoration: underline;
        }
        p, ul {
            width: 80%;
            margin: auto;
            margin-bottom: 20px;
        }
        main, footer {
            padding: 30px 20px;
        }
        button{
            font-size: 15px;
        }
        ul li>a::after {
            content: attr(href);
            padding-left: 5px;
            font-size: 12px;
            opacity: 0.5;
        }
        footer{
            opacity: 0.5;
        }
        
        .video{
            display: block;
            width: 100%;
            padding: 150px 10px;
            margin: 10px 0;
            background-color: #1f1f1f;
            text-align: center;
        }
        .video a, .video button{
            display: inline-block;
            padding: 10px;
            margin: 20px 0;
            color: white;
            background-color: #383838;
            border: 0;
            border-radius: 5px;
            font-weight: bold;
            cursor: pointer;
        }
        .video button:hover{
            text-decoration: underline;
        }
        .video>iframe{
            width: 100%;
            height: 500px;
            border: 0;
            margin-bottom: -5px;
        }
    </style>
</head>

<body>
    <header>
        <h1>Comment signer le RIP ?</h1>
    </header>
    <main>
        <p>Pour signer le RIP, il faut remplir le formulaire officiel disponible sur <a href="https://www.referendum.interieur.gouv.fr/soutien/etape-1" target="_blank" rel="noopener noreferrer">www.referendum.interieur.gouv.fr</a>.</p>
        <p>Si vous avez des difficultés pour signer, il existe de nombreuses aides en ligne. Voici différents liens :</p>
        <ul>
            <li><a href="https://www.interieur.gouv.fr/RIP/Referendum-d-initiative-partagee-foire-aux-questions" target="_blank" rel="noopener noreferrer">La foire aux questions du ministère de l'Intérieur concernant le RIP</a></li>
            <li><a href="https://www.interieur.gouv.fr/RIP/Referendum-d-initiative-partagee-le-tutoriel" target="_blank" rel="noopener noreferrer">Le tutoriel video créé par le ministère de l'Intérieur</a><br/>
                <div class="video">
                    <button data-iframe="https://www.youtube-nocookie.com/embed/3e3Z-MgFG5k"><i class="fa fa-play" aria-hidden="true"></i> Lancer la vidéo ici</button><br/>
                    <a href="https://www.youtube.com/watch?v=3e3Z-MgFG5k" target="_blank" rel="noopener noreferrer"><i class="fa fa-external-link" aria-hidden="true"></i> Regarder la vidéo sur YouTube.com</a>
                </div>
            </li>
        </ul>
        <p style="margin-bottom:5px">Si vous n'arrivez toujours pas à signer, vous pouvez appeller directement le standard du ministère de l'Intérieur à ce numéro :<br/><strong style="font-size:2em; line-height: 55px;">01 49 27 49 27</strong></p>
        <p>Sinon, voici également de nombreuses autres ressources qui peuvent vous aider :</p>
        <ul>
            <li><a href="https://www.youtube.com/watch?v=mK4lvjq11EA" target="_blank" rel="noopener noreferrer">Le tutoriel vidéo réalisé par Le Média</a></li>
            <li><a href="https://lafranceinsoumise.fr/2019/06/13/signer-le-referendum-adp/" target="_blank" rel="noopener noreferrer">Le tutoriel de La France Insoumise</a></li>
            <li><a href="https://le-bon-sens.com/2019/08/20/video-referendum-adp-comment-signer-tuto/" target="_blank" rel="noopener noreferrer">Le tutoriel vidéo d'Antoine Léaument (responsable de la communication numérique de Jean-Luc Mélenchon)</a></li>
            <li><a href="https://www.les-crises.fr/tuto-notre-guide-pour-signer-la-petition-referendumadp/" target="_blank" rel="noopener noreferrer">Le tutoriel proposé par le site Les Crises.fr</a></li>
        </ul>
        <p>Pour finir, vous pouvez vous rabattre sur un soutien papier. En effet, vous pouvez remplir un formulaire Cerfa papier et le déposer à la mairie de la commune la plus peuplée de chaque canton, dans une circonscription administrative équivalente ou au consulat.</p>
        <ul>
            <li><a href="https://www.referendum.interieur.gouv.fr/formulaire-papier" target="_blank" rel="noopener noreferrer">En savoir plus sur le dépôt d'un soutien papier</a></li>
            <li><a href="https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15264.do" target="_blank" rel="noopener noreferrer">Télécharger le formulaire Cerfa</a></li>
        </ul>
        <p>Attention, la carte montrant les mairies où un dépôt papier est possible peut être incorrecte. Certaines mairies indiquées ne reçoivent pas les formulaires papiers (notamment dans les communes fusionnées). Nous vous conseillons d'appeler la mairie avant d'aller déposer un soutien.</p>
    </main>
    <footer>
        <p>
            <a href="https://signer.rip"><i class="fa fa-arrow-left" aria-hidden="true"></i> Revenir à www.signer.rip</a>
        </p>
        <p>
            Un site web proposé par <a href="https://framagit.org/DavidLibeau/signer.rip" target="_blank" rel="noopener noreferrer">David Libeau</a>
        </p>
    </footer>
    <script src="//dav.li/jquery/3.4.1.min.js"></script>
    <script>
        $(".video>button").click(function(){
            console.log($(this).attr("data-iframe"));
            if($(this).attr("data-iframe")){
                $(this).parent().css("padding",0);
                $(this).parent().html('<iframe src="'+$(this).attr("data-iframe")+'?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
            }
        })
    </script>
</body>
</html>