<!DOCTYPE html>

<html lang="fr">

<head>
    <?php
    $titre="Signer le #RéférendumADP";
    $desctription="Lien de partage du référendum d'initiative partagée contre la privatisation d'ADP. Cliquez pour signer !";
    ?>
    <meta charset="utf-8" />
    <title><?php echo($titre); ?></title>
    <meta property="og:title" content="<?php echo($titre); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://signer.rip/" />
    <meta property="og:image" content="https://signer.rip/image.png" />
    <meta property="og:description" content="<?php echo($desctription); ?>" />
    <meta name="description" content="<?php echo($desctription); ?>" />
    <meta name="keywords" content="signer, référendum, ADP, RIP, aéroport de paris, aéroport, paris, privatisation" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@DavidLibeau" />
    <meta name="twitter:creator" content="@DavidLibeau" />
    <meta name="twitter:title" content="<?php echo($titre); ?>" />
    <meta name="twitter:description" content="<?php echo($desctription); ?>" />
    <meta name="twitter:image" content="https://signer.rip/image.png" />
    <meta property="og:locale" content="fr_FR" />
    <link rel="shortcut icon" type="image/png" href="twemoji-signer.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="theme-color" content="#3c3c3c" />
    <link rel="stylesheet" href="https://dav.li/forkawesome/1.1.7/css/fork-awesome.min.css" />
    <style>
        body, html {
            margin: 0;
            font-family: sans-serif;
            background-color: #fdfdfd;
            line-height: 20px;
        }
        header {
            padding: 25vh 20px;
            background-color: rgba(78,110,186,1);
            -webkit-box-shadow:inset 0 0 50px 0 rgba(0,0,0,0.2);
            box-shadow:inset 0 0 50px 0 rgba(0,0,0,0.2);
        }
        header h1{
            text-align: center;
            line-height: 40px;
        }
        header h1 div{
            display: inline-block;
            position: relative;
        }
        #btn {
            display: inline-block;
            padding: 20px;
            margin-bottom: 60px;
            color: #3c3c3c;
            background-color: #fdfdfd;
            border-radius: 20px;
            -webkit-box-shadow: 0 10px 40px -20px rgba(0,0,0,0.6);
            box-shadow: 0 10px 40px -20px rgba(0,0,0,0.6);
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }
        #btn:hover {
            -webkit-box-shadow: 0 20px 40px -20px rgba(0,0,0,0.6);
            box-shadow: 0 20px 40px -20px rgba(0,0,0,0.6);
        }
        #btn i {
            margin-left: 10px;
            margin-right: 0px;
            -webkit-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }
        header h1 #btn:hover i {
            margin-left: 15px;
            margin-right: -5px;
        }
        #comment{
            position: absolute;
            bottom: 0;
            left: 0;
            color: white;
            font-size: 0.9em;
            opacity: 0.4;
        }
        #comment:hover{
            opacity: 0.8;
        }
        #comment:hover:after{
            content: " Oui";
        }
        a {
            color: inherit;
            font-weight: bold;
            text-decoration: none;
        }
        a:hover{
            text-decoration: underline;
        }
        p, ul {
            width: 80%;
            margin: auto;
        }
        main, footer {
            padding: 30px 20px;
        }
        button{
            font-size: 15px;
        }
        #tweetButton{
            display: inline-block;
            font-weight: normal;
            text-decoration: none;
            text-align: left;
            width: min-content;
            max-width: 100%;
            border: 1px solid #1DA1F2;
            border-radius: 10px;
            background-color: #fdfdfd;
        }
        #tweetButton>span{
            display: block;
            padding: 15px;
            padding-bottom: 10px;
        }
        #tweetButton em{
            font-style: normal;
            color: #1DA1F2;
        }
        #tweetButton>button{
            display: block;
            width: max-content;
            max-width: 100%;
            padding: 15px 20px;
            padding-top: 10px;
            background: none;
            border: 0;
            border-top: 1px solid #1DA1F2;
            color: #1DA1F2;
        }
        #tweetButton:hover>button{
            text-decoration: underline;
        }
        #tweetButton img {
            height: 15px;
            margin-right: 2px;
        }
        footer{
            opacity: 0.5;
        }
    </style>
</head>

<body>
    <header>
        <h1><div><a id="btn" href="https://www.referendum.interieur.gouv.fr/soutien/etape-1" rel="noopener noreferrer">Cliquez ici pour signer le référendum ADP<i class="fa fa-arrow-right" aria-hidden="true"></i></a><a id="comment" href="https://comment.signer.rip">Des difficultés pour signer ?</a></div></h1>
    </header>
    <main>
        <p>Le lien pour signer le RIP est "<strong>https://www.referendum.interieur.gouv.fr/soutien/etape-1</strong>". Comme cette adresse est longue et difficile à mémoriser, il est difficile de la partager. Ainsi, ce site web propose un lien raccourci "<strong>signer.rip</strong>". Vous pouvez également partager "<strong>signer.rip/ici</strong>" ou encore "<strong>signer.rip/adp</strong>" pour un lien qui redirige automatiquement vers la pétition sans voir cette page.</p>
        <?php
        $tweet="J'ai signé le RIP et vous ? Signez pour le #ReferendumADP maintenant :";
        $tweet_html="J'ai signé le RIP et vous ? Signez pour le <em>#ReferendumADP</em> maintenant : <em>signer.rip</em>";
        ?>
        <p style="margin: 30px auto; text-align:center;"><a id="tweetButton" href="https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Frip-le-compteur.dav.li/&ref_src=twsrc%5Etfw&text=<?php echo(urlencode($tweet)); ?>&tw_p=tweetbutton&url=https%3A%2F%2Fsigner.rip/" target="_blank" rel="noopener noreferrer"><span><?php echo($tweet_html); ?></span><button>Partager le RIP sur les réseaux sociaux <i class="fa fa-twitter" aria-hidden="true"></i></button></a></p>
        <p>Voici l'ensemble des liens disponibles :</p>
        <ul>
            <li><strong>www.signer.rip</strong> : ce site web</li>
            <li><strong>signer.rip/ici</strong> : <a href="https://www.referendum.interieur.gouv.fr/soutien/etape-1" target="_blank" rel="noopener noreferrer">https://www.referendum.interieur.gouv.fr/soutien/etape-1</a></li>
            <li><strong>signer.rip/adp</strong> : <a href="https://www.referendum.interieur.gouv.fr/soutien/etape-1" target="_blank" rel="noopener noreferrer">https://www.referendum.interieur.gouv.fr/soutien/etape-1</a></li>
            <li><strong>signer.rip/cerfa</strong> : <a href="https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15264.do" target="_blank" rel="noopener noreferrer">https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15264.do</a></li>
            <li><strong>signer.rip/papier</strong> : <a href="https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15264.do" target="_blank" rel="noopener noreferrer">https://www.formulaires.modernisation.gouv.fr/gf/cerfa_15264.do</a></li>
            <li><strong>signer.rip/mairies</strong> : <a href="https://www.referendum.interieur.gouv.fr/formulaire-papier" target="_blank" rel="noopener noreferrer">https://www.referendum.interieur.gouv.fr/formulaire-papier</a></li>
        </ul>
    </main>
    <footer>
        <p>
            Un site web proposé par <a href="https://framagit.org/DavidLibeau/signer.rip" target="_blank" rel="noopener noreferrer">David Libeau</a>
        </p>
    </footer>
</body>
</html>